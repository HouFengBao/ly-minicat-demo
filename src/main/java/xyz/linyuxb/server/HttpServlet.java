package xyz.linyuxb.server;

/**
 * @Author: linyuxb
 * @Date: 2021/11/15 4:34 下午
 * @Desc: Servlet 抽象实现
 */
public abstract class HttpServlet implements Servlet {
    /**
     * 执行get请求
     *
     * @param request
     * @param response
     */
    public abstract void doGet(Request request, Response response);

    /**
     * 执行post请求
     *
     * @param request
     * @param response
     */
    public abstract void doPost(Request request, Response response);

    /**
     * 服务处理方法
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @Override
    public void service(Request request, Response response) throws Exception {
        if ("GET".equalsIgnoreCase(request.getMethod())) {
            doGet(request, response);
        } else {
            doPost(request, response);
        }
    }
}
