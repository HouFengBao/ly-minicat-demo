package xyz.linyuxb.server;

/**
 * @Author: linyuxb
 * @Date: 2021/11/15 3:33 下午
 * @Desc: 自定义Servlet
 */
public interface Servlet {
    /**
     * 初始化方法
     *
     * @throws Exception
     */
    void init() throws Exception;

    /**
     * 销毁方法
     *
     * @throws Exception
     */
    void destory() throws Exception;

    /**
     * 服务处理方法
     *
     * @param request
     * @param response
     * @throws Exception
     */
    void service(Request request, Response response) throws Exception;
}
