package xyz.linyuxb;

import xyz.linyuxb.server.Bootstrap;

/**
 * @Author: linyuxb
 * @Date: 2021/11/15 3:33 下午
 * @Desc: MinCat启动类
 */
public class MiniCatMain {
    public static void main(String[] args) throws Exception {
        new Bootstrap().start();
    }
}
